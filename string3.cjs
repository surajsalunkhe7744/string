// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.


//var date = "11/1/2021";



function monthDate(date)
{
    if(typeof date != 'string')
         return "";
    else
    {
        let temp = new Date(date);
        let res = temp.getMonth();
        return res+1;
    }
}

//console.log(monthDate(date));

module.exports = monthDate;