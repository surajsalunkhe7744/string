// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. 
//Write a function to convert the given strings into their equivalent numeric format 
//without any precision loss - 100.45, 1002.22, -123 and so on. 
// There could be typing mistakes in the string so if the number is invalid, return 0.

//let num = "-$412145.0s";
//let fl;
function numCheck(num)
{
    if(typeof(num) != 'string')
    return 0;

    let temp="";
    for(var i=0;i<=num.length-1;i++)
    {
        if(num.charAt(i)!='$')
        temp=temp+num.charAt(i);
    }
    
        let res = +(temp);

        if(isNaN(res))
        return 0;
        else
        return res;
}

//console.log(numCheck(num));

module.exports = numCheck;


