// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function con(arrString)
{
    if(!(Array.isArray(arrString)) || arrString.length ==0)
         return "";
    else
         {
             if(arrString.length==1)
             {
                let temp = arrString[0]+"";
                return temp;
             }
             else
             {
                let temp="";
                for(var x=0;x<=arrString.length-1;x++)
                {
                     temp=temp+arrString[x]+" ";
                }
                return temp;
             }
         }

}

module.exports = con;