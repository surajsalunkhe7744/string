// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 
// and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.


//const add = "111.139.161.-143";

const t = [];

function addArray(add)
{
    if(typeof add === 'string')
    {
        var temp=add.split(".");

        if(temp.length>4 || temp.length<4)
        return t;

        for(var i=0;i<=temp.length-1;i++)
        {
            var k=Number(temp[i]);
            if(k == NaN || k<0 || k>255)
            return t;
            else
            temp[i]=k;
        }
        return temp;
    }
    return t;
    
}

//console.log(addArray(add));

module.exports = addArray;


