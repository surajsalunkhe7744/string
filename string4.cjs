// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

//var nameObj = {"first_name": "joHN","last_name": "SMith"}
function titleCase(nameObj)
{
    if(typeof nameObj != 'object')
         return "";
    else
         {
            if(nameObj.hasOwnProperty("first_name"))
            {
             if(nameObj.first_name!=null)
             {
                let temp=nameObj.first_name;
                let x=temp.substring(0,1);
                let y=temp.substring(1,temp.length);
                let a=x.toUpperCase();
                let b=y.toLowerCase();
                let out = a+b;
                 nameObj["first_name"] = out;
             }
            }
            
            if(nameObj.hasOwnProperty("middle_name"))
            {
             if(nameObj.middle_name!=null)
             {
                let temp=nameObj.middle_name;
                let x=temp.substring(0,1);
                let y=temp.substring(1,temp.length);
                let a=x.toUpperCase();
                let b=y.toLowerCase();
                let out = a+b;
                nameObj["middle_name"] = out;
             }
            }
            
            if(nameObj.hasOwnProperty("last_name"))
            {
             if(nameObj.last_name!=null)
             {
                let temp=nameObj.last_name;
                let x=temp.substring(0,1);
                let y=temp.substring(1,temp.length);
                let a=x.toUpperCase();
                let b=y.toLowerCase();
                let out = a+b;
                nameObj["last_name"] = out;
             }
            }

            return nameObj;
         }

}

//console.log(titleCase(nameObj));

module.exports = titleCase;
